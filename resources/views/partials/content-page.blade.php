@php the_content() @endphp

<div class="container-fluid wrap" role="document">
    <div class="row mx-md-n5">
        <div class="pb-4 col-md-6 px-md-5 pb-md-0">
            <p class="large"><a href="https://www.who.int/news-room/fact-sheets/detail/tobacco" target="_blank">The tobacco epidemic is one of the biggest public health threats the world has ever faced, killing more than 8 million people a year around the world.</a> The UK has been at the forefront of initiating many forward-thinking policies for helping smokers quit, and is one few countries who have successfully brought the adult <a href="https://www.ons.gov.uk/peoplepopulationandcommunity/healthandsocialcare/healthandlifeexpectancies/bulletins/adultsmokinghabitsingreatbritain/2019" target="_blank">smoking rates down to 14.1% (2019)</a>. It is great to see Public Health England and other national health authorities supporting Tobacco Harm Reduction (THR) tools, along with behavioural therapy and other licenced medications, in order to provide more choice of cessation products, to help smokers quit smoking.  </p>
        </div>
        <div class="col-md-6 px-md-5">
            <p>In July 2019, the UK Government set out its ambition for England to be <a href="https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/630217/Towards_a_Smoke_free_Generation_-_A_Tobacco_Control_Plan_for_England_2017-2022__2_.pdf" target="_blank">“smokefree by 2030”</a>. Despite being the world leader in forward thinking tobacco control policies  and having a range of smoking cessation tools available with free support, we are seeing that the decrease in smoking prevalence in the UK is plateauing. Also some specific subgroups of the population have higher smoking rates than the national average. E.g. while a decrease in smoking rates has also been seen among adults with a long-term mental health condition – falling from 35.3% (in 2013 to 2014) to <a href="https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/630217/Towards_a_Smoke_free_Generation_-_A_Tobacco_Control_Plan_for_England_2017-2022__2_.pdf" target="_blank">26.8% (in 2018</a> to 2019) – prevalence remains substantially higher, despite the same levels of motivation to quit.</p>
            <p>Now it is predicted that the Government is likely to miss its ‘Smokefree by 2030’ target. Projections undertaken by <a href="https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/630217/Towards_a_Smoke_free_Generation_-_A_Tobacco_Control_Plan_for_England_2017-2022__2_.pdf" target="_blank">CRUK in Feb 2020 predicted that it will take until the mid-2040s to achieve smoking rates of less than 5% among the most deprived communities in England.</a></p>
            <p>The UK health authorities have made multiple recommendations to provide additional support to specific groups of people who smoke. But despite this, rates of smoking in pregnant women hasn’t significantly gone down and smoking is still the biggest contributor to decreased life span in people with mental health conditions.</p>
        </div>
    </div>
</div>

<div class="pink-bg">
    <div class="container-fluid wrap" role="document">
        <div class="row mx-md-n5">
            <div class="col-md-6 px-md-5">
                <h2 class="darkblue in-the-uk-title"><a target="_blank" href="https://www.ons.gov.uk/peoplepopulationandcommunity/healthandsocialcare/healthandlifeexpectancies/bulletins/adultsmokinghabitsingreatbritain/2019">In 2019, in the UK:</a></h2>

                <ul class="list">
                    <li>Around 6.9 million people in the adult population still smoked.</li>
                    <li>15.9% of men smoke compared with 12.5% of women.</li>
                    <li>Those aged 25 to 34 years had the highest proportion of current smokers (19.0%)</li>
                </ul>
            </div>
            <div class="col-md-6 px-md-5">
                
                <ul class="list">
                    <li><a target="_blank" href="https://files.digital.nhs.uk/D9/5AACD3/smok-eng-2019-rep.pdf">10.6% of pregnant women were known to be smokers at the time of delivery</a> in 2018/19. This is similar to 2017/18 (10.8%)</li>
                    <li><a target="_blank" href="https://files.digital.nhs.uk/D9/5AACD3/smok-eng-2019-rep.pdf">489,300 hospital admissions</a> attributable to smoking in 2017-18, and this is 1% increase compared to previous year. 77,800 deaths attributable to smoking (2017), and this is similar to previous year.</li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid wrap" role="document">
    <div class="row mx-md-n5">
        <div class="col-md-6 px-md-5">
            <p class="large">At the Centre for Health Research and Education (CHRE) we are passionate about preventive medicine. Our mantra is to ‘Bridge the policy and Practice Gap in healthcare’. In our smoking cessation work, we are product agnostic and do not favour any particular category of cessation tool.</p>
        </div>
        <div class="col-md-6 px-md-5">
            <p>To understand why there is a gap between UK’s policies and what is happening in practice, we needed to understand the challenges that the remaining smokers in UK are facing. To know these barriers, we conducted a quantitative research study on a sample of UK’s remaining smokers and their influencers, i.e. professionals who might have a positive influence on smoker’s quitting journey. This quantitative survey was preceded by qualitative research conducted by CHRE.</p>
            <p>The ‘Smoke-free UK’ project was conducted to answer the question: ‘Who are the remaining ‘hard core’ of smokers in the UK and what are the drivers and barriers to quit among them?’.</p>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <p>CHRE commissioned this site to openly and transparently share findings from the Smoke-free UK project with researchers.<br class="responsive-br"> 
            We hope that this will facilitate the acceleration of UK’s journey in becoming smoke-free.</p>
        </div>
    </div>
</div>

<div class="container-fluid wrap no-padd">
    <hr>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-7">
            <h3 class="green pb-36">Funding declaration:</h3>
            <p>This study is conceptualised, designed, led and managed by CHRE. CHRE applied for and was awarded a grant from the ‘Foundation for a SmokeFree World (FSFW)’ for this project.</p>
        </div>
    </div>
</div>