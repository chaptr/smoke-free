<header class="banner">
  <div class="container-fluid">
    <div class="row">
      <div class="col-4 col-md-6">
        <a target="_blank" href="https://www.chre-uk.com">@include('svg.logo')</a>
      </div>
      <div class="col-8 col-md-6 button-col">
        @if(is_front_page())
          <a href="/the-smoke-free-uk-project" class="button" data-toggle="modal" data-target="#exampleModal">Smokefree UK Project</a>
        @else
          <a href="/" class="button">@include('svg.arrow')&nbsp;&nbsp;Introduction</a>
        @endif
      </div>
    </div>
  </div>
</header>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title darkblue" id="exampleModalLabel">Please sign up to view details of The Smoke-Free UK Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        @php 
          echo do_shortcode( '[contact-form-7 id="12" title="Modal pop-up"]' );
        @endphp 

      </div>
    </div>
  </div>
</div>