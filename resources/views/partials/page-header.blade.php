<div class="container-fluid wrap intro" role="document">
    <div class="page-header">
        <h1 class="darkblue">@php echo the_field('page_title') @endphp</h1>
    </div>
</div>
