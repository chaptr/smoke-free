<footer class="content-info">
  <div class="container-fluid">

      <?php 
      $link = get_field('footer_link_one', 'option');
      if( $link ): 
          $link_url = $link['url'];
          $link_title = $link['title'];
          $link_target = $link['target'] ? $link['target'] : '_self';
          ?>
          <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
      <?php endif; ?>

      <?php 
      $link_two = get_field('footer_link_two', 'option');
      if( $link_two ): 
          $link_two_url = $link_two['url'];
          $link_two_title = $link_two['title'];
          $link_two_target = $link_two['target'] ? $link_two['target'] : '_self';
          ?>
          <a class="button" href="<?php echo esc_url( $link_two_url ); ?>" target="<?php echo esc_attr( $link_two_target ); ?>"><?php echo esc_html( $link_two_title ); ?></a>
      <?php endif; ?>
                    
  </div>
</footer>
