<div class="table pt-0 container-fluid wrap">
    <div class="row">
        <div class="col">
            <div class="medium">@php echo the_sub_field('title') @endphp </div>
            <table>
                                   
                @if( have_rows('table_rows') )
                    @php $table = 0; @endphp 
                    @while( have_rows('table_rows') ) 
                    @php the_row(); @endphp     
                        
                    
                            @if($table == 3) 
                                <tr>
                                    <td>
                                        @if (get_sub_field('column_one'))
                                            @php echo get_sub_field('column_one'); @endphp 
                                        @endif 
                                    </td> 
                            @else 
                                <thead>
                                    <tr>
                                        <th>
                                            @if (get_sub_field('column_one'))
                                                @php echo get_sub_field('column_one'); @endphp 
                                                @php $table++ @endphp 
                                            @endif 
                                        </th>
                            @endif
                            @if($table == 3) 
                                <td>
                                    @if (get_sub_field('column_two'))
                                    @php echo get_sub_field('column_two'); @endphp 
                                    @endif 
                                </td> 
                            @else 
                                <th>
                                    @if (get_sub_field('column_two'))
                                        @php echo get_sub_field('column_two'); @endphp 
                                        @php $table++ @endphp 
                                    @endif 
                                </th>
                            @endif
                            @if($table == 3) 
                                    <td>
                                        @if (get_sub_field('column_three'))
                                        @php echo get_sub_field('column_three'); @endphp 
                                        @endif 
                                    </td> 
                                </tr>
                            @else 
                                        <th>
                                            @if (get_sub_field('column_three'))
                                                @php echo get_sub_field('column_three'); @endphp 
                                                @php $table++ @endphp 
                                            @endif 
                                        </th>
                                    </tr>
                                </thead>
                            @endif
                        </tr>
                    </thead>

                    @endwhile
                @endif 
                 
            </table>
        </div>
    </div>
</div>