<div @if(get_sub_field('coloured_background')==1) class="pink-bg" @endif>
    <div class="container-fluid wrap" role="document">
        @if(get_sub_field('title'))
            @if(get_sub_field('title_size')==1) <h3 class="small-heading">@php echo the_sub_field('title') @endphp</h3> @else <h2 class="darkblue in-the-uk-title">@php echo the_sub_field('title') @endphp </h2> @endif
        @endif
        <div class="row mx-md-n5">
            <div class="pb-4 col-md-6 px-md-5 pb-md-0">
                @if( have_rows('left_column') )
                    @while ( have_rows('left_column') )
                        @php the_row() @endphp
                        @if( get_row_layout() == 'text' )
                            <div @if(get_sub_field('text_size')==1) class="large" @endif>@php echo the_sub_field('content') @endphp</div>
                        @elseif( get_row_layout() == 'list' )
                        @endif
                    @endwhile
                @endif
            </div>
            <div class="col-md-6 px-md-5">
                @if( have_rows('right_column') )
                    @while ( have_rows('right_column') )
                        @php the_row() @endphp
                        @if( get_row_layout() == 'text' )
                            <div @if(get_sub_field('text_size')==1) class="large" @endif>@php echo the_sub_field('content') @endphp</div>
                        @endif
                    @endwhile
                @endif
            </div>
        </div>
    </div>
</div>