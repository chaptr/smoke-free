@if(have_rows('buttons'))
    <div class="buttons container-fluid">
        @while(have_rows('buttons'))
            @php the_row() @endphp
            <a class="button" target="{{get_sub_field('link')['target']}}" href="{{get_sub_field('link')['url']}}">{{get_sub_field('link')['title']}}</a>
        @endwhile
    </div>
@endif