<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @php echo the_sub_field('content') @endphp
        </div>
    </div>
</div>