<div class="table pt-0 container-fluid wrap">
    <div class="row mx-md-n5">
        <div class="col-xl-8 px-xl-5">

            @if( have_rows('fraction_columns_one') )
                @while ( have_rows('fraction_columns_one') )
                    @php the_row() @endphp
                    
                    @if( get_row_layout() == 'text' )
                        <div @if(get_sub_field('text_size')==1) class="large" @endif>@php echo the_sub_field('content') @endphp</div>
                    @elseif( get_row_layout() == 'list' )

                    
                    @endif
                    
                @endwhile
            @endif

        </div>
        
        <div class="col-xl-4 px-xl-5 button-col">


            @php $count = 0; @endphp
            
            @if( have_rows('fraction_columns_two') )
                @while ( have_rows('fraction_columns_two') )
                    @php the_row() @endphp
                    
                    @if( get_row_layout() == 'fraction_buttons' )
                    
                        @if( have_rows('buttons') )
                            @while ( have_rows('buttons') ) 
                                @php the_row() @endphp 

                                @php $count++ @endphp 

                                @php 
                                $link = get_sub_field('button');
                                if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    @endphp 

                                    <div>
                                        <a class="button @if($count == 2) alt @endif" href="@php echo esc_url( $link_url ); @endphp" target="@php echo esc_attr( $link_target ); @endphp">@php echo esc_html( $link_title ); @endphp</a>
                                        {{-- @php var_dump($count) @endphp  --}}
                                        
                                    </div>
                                    
                                <?php endif; ?>
                                
            
                            @endwhile
                        @endif
                    
                    @endif  
                @endwhile
            @endif
            
        </div>
    </div>
</div>