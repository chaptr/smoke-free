@php the_content() @endphp

<div class="container-fluid wrap">
    <div class="row mx-md-n5">
        <div class="col-md-6 px-md-5">
            <p>CHRE conducted a literature review, semi structured interviews and a wide range of qualitative research focus groups to confirm that UK's remaining hard core of smokers come primarily from sub populations that are socio economically and/or health-wise disadvantaged. It’s analysis gave some deep insights into the challenges faced by smokers and their influencers.</p>
            <p>We noted that smokers do not have reliable and consistent advice when it comes to smoking cessation.  Product availability and assurance are also an important factor- e-cigarettes are actively endorsed in the UK at the policy level, however, at the practice level the message is not often translated as well. We found that those in healthcare who are in a position to advice smokers are often ill-equipped or simply do not have the time or incentive to do so.</p>
        </div>
        <div class="col-md-6 px-md-5">
            <p>We then conducted a quantitative survey, to confirm the findings and then make recommendations based on those. CHRE is product agnostic and does not favour any one category of cessation tool. To the best of our knowledge, we included questions to get insights on all smoking cessation tools available in UK.  As this study was conducted in 2020, when the Covid-19 pandemic hit the world, we also included a subset of questions to understand changes to smoking behaviour due to the pandemic.</p>
            <p>CHRE is in the process of analysing the data from the quantitative study and as the results and reports are available, shall make them available on this site.</p>
            <p>CHRE were supported by Ogilvy Consulting's Behavioural Science Practice in the design and analysis of the quantitative phase of the Smoke-free UK project.</p>
        </div>
    </div>
</div>

<div class="container-fluid wrap no-padd">
    <hr />
</div>

<div class="container-fluid wrap">
    <h3 class="small-heading">Methods</h3>
    <div class="row mx-md-n5">
        <div class="col-md-6 px-md-5 mob-padd-bottom">
            <h4 class="large green padd-bottom">Survey Design</h4>
            <p>Two surveys were created with an objective of better understanding smoking cessation experiences in the UK, from the perspective of current and former smokers (referred to as ‘consumers’ throughout), and certain professionals who may influence them (referred to as ‘influencers’ throughout). These influencers include health care professionals (HCPs) from a range of professions, stop smoking advisors (SSAs), and vape shop employees and owners. In addition, beauticians, social workers and barbers/hairdressers, were included in the influencer group; while not traditional influencers in this space these are professions where there are often regular encounters with the same members of the public.</p>
            <p>Questions were developed to understand the knowledge, attitudes and practice of influencers, and the experiences and motivations of consumers. Full details of all questions can be found in the Appendix. These questions were informed by key findings from qualitative research as part of CHRE’s Smokefree Landscape Project.</p>
        </div>
        <div class="col-md-6 px-md-5">
            <h4 class="large green padd-bottom">Participant Classification</h4>
            <p>Influencers were identified by their reported profession and grouped together, as shown in Table 1A. For the purposes of this project Stop Smoking Advisors (SSAs) are on some occasions grouped with HCPs, and on others treated separately based on the question – this is noted each time. All other influencers are referred to as ‘non-HCPs’.</p>
        </div>
    </div>
</div>

<div class="table pt-0 container-fluid wrap">
    <div class="row">
        <div class="col">
            <p class="medium"><span class="green">Table 1A:</span> Influencers by profession and grouping</p>

            <table>
                <thead>
                    <tr>
                        <th>
                            Profession
                        </th>
                        <th>
                            Number of Influencer Respondents
                        </th>
                        <th>
                            Categorisation
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            GP
                        </td>
                        <td>
                            101
                        </td>
                        <td>
                            Health Care Professional (HCP)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            GP Surgery Practice Nurse (PN)
                        </td>
                        <td>
                            51
                        </td>
                        <td>
                            HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Midwife
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Community Mental Health Nurse (CMHN)
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Dentist
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Pharmacist
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Respiratory Specialist
                        </td>
                        <td>
                            75
                        </td>
                        <td>
                            HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Stop Smoking Advisor (SSA)
                        </td>
                        <td>
                            50
                        </td>
                        <td>
                            -
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Social Worker
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            Non-HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Hairdresser/barber
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            Non-HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Beautician
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            Non-HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vape Shop Employee/Owner
                        </td>
                        <td>
                            50
                        </td>
                        <td>
                            Non-HCP
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="medium">Total</span>
                        </td>
                        <td>
                            <span class="medium">1,027</span>
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="table pt-0 container-fluid wrap">
    <div class="row">
        <div class="col">
            <p class="medium"><span class="green">Table 1B:</span> Consumer Categorisation<br />Q: Which of the following best describes your smoking status?</p>
            <table>
                <thead>
                    <tr>
                        <th>
                            Consumer Group
                        </th>
                        <th>
                            Definition
                        </th>
                        <th>
                            Number of Consumers
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Current smokers
                        </td>
                        <td>
                            Self-described as: ‘I currently smoke regularly’
                        </td>
                        <td>
                            301
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Dual users
                        </td>
                        <td>
                            Self-described as: ‘I currently smoke and vape regularly’
                        </td>
                        <td>
                            245
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ex-smoker now vaper
                        </td>
                        <td>
                            Self-described as: ‘I used to smoke regularly, but now I only vape’
                        </td>
                        <td>
                            244
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ex-smoker
                        </td>
                        <td>
                            Self-described as: 'I used to smoke regularly, but now don’t smoke or vape'
                        </td>
                        <td>
                            164
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="medium">Total</span>
                        </td>
                        <td>
                            <span class="medium">All respondents</span>
                        </td>
                        <td>
                            <span class="medium">954</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="table pt-0 container-fluid wrap">
    <div class="row mx-md-n5">
        <div class="col-xl-8 px-xl-5">
            <p>We will soon be publishing our reports based on these data.</p>
            <p>As part of this wider project we were also interested in understanding the experiences of smokers with mental health conditions, and women who smoked while they were pregnant and specifically selected a quota of respondents within these two groups. From our smoker group of 590 participants, 203 had a mental health condition, and 54 women smoked during their pregnancy once a week or more while pregnant.Their experiences will be discussed specifically in separate reports, also to be later published on this site.</p>
        </div>
        <div class="col-xl-4 px-xl-5 button-col">
            <div>
                <a href="{{ get_template_directory_uri() . '/pdfs/quant-survey-influencers-final-questionnaire.pdf' }}" target="_blank" class="button">Download Questionnaire One</a>
            </div>
            <div>
                <a href="{{ get_template_directory_uri() . '/pdfs/quant-survey-consumers-final-questionnaire.pdf' }}" target="_blank" class="button alt">Download Questionnaire Two</a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid wrap no-padd">
    <hr />
</div>

<div class="container-fluid wrap big">
    <div class="row mx-md-n5">
        <div class="col-md-6 px-md-5 mob-padd-bottom">
            <h4 class="large green padd-bottom">Ethics Approvals</h4>
            <p>It was confirmed with the Health Research Authority that HRA and HCRW Approval was not required for this study. This was because the individuals who had consented to be contacted for further research were assessed, as opposed to recruiting through NHS organisations.</p>
        </div>
        <div class="col-md-6 px-md-5">
            <h4 class="large green padd-bottom">Data Collection</h4>
                <p>Both the influencer and consumer surveys were conducted online and implemented by Atomik Research, using their panellists throughout the UK. Atomik Research, part of the 4Media Group, is an independent creative market research agency that employs MRS-certified researchers and abides to MRS code. The influencer survey was infield from the 15th of May 2020 to the 29th of May 2020, while the consumer survey was infield from the 15th of May to the 3rd of June 2020. During this time, the UK was under lockdown due to COVID-19 and had been since late March. The infield date of the surveys also coincided with the ban of menthol cigarettes in the UK as of May 20th.</p>
                <p>For both surveys, respondents were directed to questions based on their previous answers and categorisation, therefore the number of respondents varied per question and is often less than the total number of participants.</p>
        </div>
    </div>
</div>

{{-- <div class="pt-0 container-fluid wrap">
    <div class="row mx-md-n5">
        <div class="col-md-6 px-md-5 mob-padd-bottom">

        </div>
        <div class="col-md-6 px-md-5">
            <h4 class="large green padd-bottom">Ethics Approvals</h4>
            <p>It was confirmed with the Health Research Authority that HRA and HCRW Approval was not required for this study. This was because the individuals who had consented to be contacted for further research were assessed, as opposed to recruiting through NHS organisations.</p>
        </div>
    </div>
</div> --}}