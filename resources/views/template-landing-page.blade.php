{{--
  Template Name: Landing Page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @if( have_rows('content_blocks') )
      @php $i = 1; @endphp
      @while ( have_rows('content_blocks') )
          @php $i++; @endphp
          @php
              $component = the_row();
          @endphp
          @include('partials.section')
      @endwhile
    @endif
  @endwhile
@endsection
